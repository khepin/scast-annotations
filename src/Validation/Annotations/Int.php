<?php

namespace Validation\Annotations;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Int {
    function validator() {
        return new \Validation\Int();
    }
}