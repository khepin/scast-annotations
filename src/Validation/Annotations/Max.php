<?php

namespace Validation\Annotations;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Max {

    private $max;

    function __construct($values) {
        $this->max = $values['value'];
    }

    function validator() {
        return new \Validation\Max($this->max);
    }
}