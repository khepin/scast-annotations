<?php

namespace Validation;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

class AnnotationValidator {

    private $reader;

    function __construct() {
        AnnotationRegistry::registerLoader(function($name){
            return class_exists($name, true);
        });

        $this->reader = new AnnotationReader();
    }

    function isValid($object) {
        $reflClass = new \ReflectionClass($object);

        foreach($reflClass->getProperties() as $prop) {
            $annotations = $this->reader->getPropertyAnnotations($prop);

            foreach ($annotations as $annotation) {
                $validator = $annotation->validator();

                $isValid = $validator->isValid($prop->getValue($object));

                if (!$isValid) {
                    return false;
                }
            }
        }

        return true;
    }
}