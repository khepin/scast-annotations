<?php
namespace Validation;

class Max {

    private $max;

    function __construct($max) {
        $this->max = $max;
    }

    function isValid($value) {
        return $value <= $this->max;
    }
}