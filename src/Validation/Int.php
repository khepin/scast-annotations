<?php
namespace Validation;

class Int {
    function isValid($value) {
        return is_int($value);
    }
}