**Hello**
Hi everyone, I'm gonna show you how to use annotations in PHP

* Explain validation project and show validators (Int, Min, Max)
* Show class with annotations
* Install Doctrine annotations
* Create Annotations Folder and Annotation classes:

classes: 

	<?php
	namespace Validation\Annotations;
	use Validation\Int as IntValidator;

	/**
	 * @Annotation
	 * @Target("PROPERTY")
	 */
	class Int implements ValidationAnnotation {
		function validator() {
			return new IntValidator();
		}
	}

	<?php
	namespace Validation\Annotations;

	use Validation\Max as MaxValidator;

	/**
	 * @Annotation
	 * @Target("PROPERTY")
	 */
	class Max implements ValidationAnnotation {
		private $max;

		function __construct($args) {
			$this->max = $args['value'];
		}

		function validator() {
			return new MaxValidator($this->max);
		}
	}

## validator

	<?php

	namespace Validation;

	use Doctrine\Common\Annotations\AnnotationReader;
	use Doctrine\Common\Annotations\AnnotationRegistry;
	use Validation\Annotations\ValidationAnnotation;

	class AnnotationValidator implements Validator {

		private $reader;

		function __construct() {
			AnnotationRegistry::registerLoader(function(){return true;});
			$this->reader = new AnnotationReader();
		}

		function isValid($object) {
			$refl = new \ReflectionClass($object);

			foreach($refl->getProperties() as $reflAttr) {
				$annotations = $this->reader->getPropertyAnnotations($reflAttr);

				foreach ($annotations as $annotation) {
					if ($annotation instanceof ValidationAnnotation) {
						$valid = $annotation->validator()->isValid($reflAttr->getValue($object));

						if ($valid === false) {
							return $valid;
						}
					}
				}
			}
			return true;
		}
	}

# go back to add interface

	<?php
	namespace Validation\Annotations;

	interface ValidationAnnotation {
		function validator();
	}

# test

	function testAnnotationValidator() {
		require 'fixtures.php';
		$school = new School(7.8);

		$validator = new AnnotationValidator();

		$this->assertTrue($validator->isValid($school));
	}