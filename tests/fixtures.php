<?php

use Validation\Annotations as Assert;

class School {

	const CAPACITY = 100;

	/**
	 * Number of students enrolled in this school
	 * @var integer
	 *
	 * @Assert\Int
	 * @Assert\Max(School::CAPACITY)
	 */
	public $totalStudents = 0;

	function __construct($students) {
		$this->totalStudents = $students;
	}
}