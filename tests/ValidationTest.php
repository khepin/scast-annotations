<?php

use Validation\Int;
use Validation\Max;
use Validation\AnnotationValidator;

class ValidationTest extends PHPUnit_Framework_TestCase {

	function testIntValidator() {
		$validator = new Int();
		$this->assertTrue($validator->isValid(3));
		$this->assertTrue($validator->isValid(39));
		$this->assertTrue($validator->isValid(39*678));

		$this->assertFalse($validator->isValid(8.6));
		$this->assertFalse($validator->isValid('hello'));
		$this->assertFalse($validator->isValid(new Int()));
		$this->assertFalse($validator->isValid(function(){}));
	}

	function testMaxValidator() {
		$validator = new Max(6);

		$this->assertTrue($validator->isValid(4));
		$this->assertTrue($validator->isValid(4.6));

		$this->assertFalse($validator->isValid(6.1));
	}

	function testAnnotationValidator() {
		require 'fixtures.php';

		$validator = new AnnotationValidator();

		$this->assertTrue($validator->isValid(new School(6)));

		$this->assertFalse($validator->isValid(new School(5.67)));

		$this->assertTrue($validator->isValid(new School(99)));
		$this->assertFalse($validator->isValid(new School(1000)));
	}
}